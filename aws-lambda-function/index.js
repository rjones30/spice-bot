'use strict';
var awsIot = require('aws-sdk');
var s3 = new awsIot.S3();
/**
* change these parameters based on what AWS Account you're using
**/
var bucket = "unique-bucket-name";
var recipeFolderPath = "recipes/";
var endpoint = "amazon-endpint";
var dispenseTopic = "test";
var topicARN = "amazon-topic-arn"
// --------------- Main handler -----------------------
// Route the incoming request based on type (LaunchRequest, IntentRequest,
// etc.) The JSON body of the request is provided in the event parameter.
exports.handler = function (event, context) {
    try {
        console.log("event.session.application.applicationId=" + event.session.application.applicationId);
		 /**
		 * Uncomment this if statement and populate with your skill's application ID to
		 * prevent someone else from configuring a skill that sends requests to this function.
		 */
		/*
		if (event.session.application.applicationId !== 'amzn1.echo-sdk-ams.app.[unique-value-here]') {
			 context.fail("Invalid Application ID");
		}
		*/
        if (event.session.new) {
            onSessionStarted({ requestId: event.request.requestId }, event.session);
        }
        if (event.request.type === "LaunchRequest") {
            onLaunch(event.request, event.session, function callback(sessionAttributes, speechletResponse) {
                    context.succeed(buildResponse(sessionAttributes, speechletResponse));
                });
        } 
		else if (event.request.type === "IntentRequest") {
		    if(event.request.dialogState === "STARTED") {
		        let directive = {"type": "Dialog.Delegate"};
		        let response  = {"directives": [directive]};
                context.succeed({
                    "version": "1.0",
                    "response": response,
                    "sessionAttributes": {},
                    "shouldEndSession": false
                });
            }
            else if(event.request.dialogState === "IN_PROGRESS" && event.request.intent.name != "AddRecipe") {
                let directive = {"type": "Dialog.Delegate"};
		        let response  = {"directives": [directive]};
                context.succeed({
                    "version": "1.0",
                    "response": response,
                    "sessionAttributes": {},
                    "shouldEndSession": false
                });
            }
            else {
                if(event.request.intent.name == "AddRecipe") {
                    console.log("Add recipe intent received");
                    if (event.request.intent.slots.Recipe.value && event.request.intent.slots.SpiceCount.value) {
                        console.log("both spice and spice amount values are given");
                        onIntent(event.request,event.session, function callback(sessionAttributes, speechletResponse) {
                            context.succeed(buildResponse(sessionAttributes, speechletResponse));
                        });
                    }
                    else {
                        let directive = {"type": "Dialog.Delegate"};
        		        let response  = {"directives": [directive]};
                        context.succeed({
                            "version": "1.0",
                            "response": response,
                            "sessionAttributes": {},
                            "shouldEndSession": false
                        });
                    }
                }
                else {
                    onIntent(event.request,event.session,function callback(sessionAttributes, speechletResponse) {
                        context.succeed(buildResponse(sessionAttributes, speechletResponse));
                    });
                }
            }
        }
		else if (event.request.type === "SessionEndedRequest") {
            onSessionEnded(event.request, event.session);
            context.succeed();
        }
    } 
	catch (e) {
        context.fail("Exception: " + e);
    }
};
// Called when the session starts.
function onSessionStarted(sessionStartedRequest, session) {
    console.log("onSessionStarted requestId=" + sessionStartedRequest.requestId + ", sessionId=" + session.sessionId);
}
// --------------- Events -----------------------
// Called when the user invokes the skill without specifying what they want.
function onLaunch(launchRequest, session, callback) {
    console.log("onLaunch requestId=" + launchRequest.requestId + ", sessionId=" + session.sessionId);
    //Alexa welcome message
    getWelcomeResponse(callback);
}
// Called when the user specifies an intent for this skill.
function onIntent(intentRequest, session, callback) {
    console.log("onIntent requestId=" + intentRequest.requestId + ", sessionId=" + session.sessionId);
    var intent = intentRequest.intent,
        intentName = intentRequest.intent.name;
    // dispatch custom intents to handlers here
    if (intentName == 'TestIntent') {
        handleTestRequest(intent, session, callback);
    }
    else if (intentName === 'DispenseSpice') {
        dispenseSpice(intent, session, callback);
    }
	else if (intentName === 'DispenseRecipe') {
        dispenseRecipe(intent, session, callback);
    }
    else if (intentName === 'AddRecipe') {
        addRecipe(intent, session, callback);
    }
    else if (intentName === 'DeleteRecipe') {
        deleteRecipe(intent, session, callback);
    }
    else if (intentName === 'ReadRecipe') {
        readRecipe(intent, session, callback);
    }
    else if (intentName === 'AddSpiceToRecipe') {
        addSpiceToRecipe(intent, session, callback);
    }
    else if (intentName === 'DeleteSpiceFromRecipe') {
        deleteSpiceFromRecipe(intent, session, callback);
    }
    else if (intentName === 'UpdateSpiceAmountInRecipe') {
        updateSpiceAmountInRecipe(intent, session, callback);
    }
    else if (intentName === 'AMAZON.HelpIntent') {
        getWelcomeResponse(callback);
    } 
	else if (intentName === 'AMAZON.StopIntent' || intentName === 'AMAZON.CancelIntent') {
        handleSessionEndRequest(callback);
    }
    else {
        throw "Invalid intent";
    }
}
//Called when the user ends the session.
//Is not called when the skill returns shouldEndSession=true.
function onSessionEnded(sessionEndedRequest, session) {
    console.log("onSessionEnded requestId=" + sessionEndedRequest.requestId + ", sessionId=" + session.sessionId);
}
// ------- Helper functions to build responses -------
function buildSpeechletResponse(title, output, repromptText, shouldEndSession) {
    return {
        outputSpeech: {
            type: "PlainText",
            text: output
        },
        card: {
            type: "Simple",
            title: title,
            content: output
        },
        reprompt: {
            outputSpeech: {
                type: "PlainText",
                text: repromptText
            }
        },
        shouldEndSession: shouldEndSession
    };
}
function buildSpeechletResponseWithoutCard(output, repromptText, shouldEndSession) {
    return {
        outputSpeech: {
            type: "PlainText",
            text: output
        },
        reprompt: {
            outputSpeech: {
                type: "PlainText",
                text: repromptText
            }
        },
        shouldEndSession: shouldEndSession
    };
}
function buildResponse(sessionAttributes, speechletResponse) {
    return {
        version: "1.0",
        sessionAttributes: sessionAttributes,
        response: speechletResponse
    };
}
//Helper function to get recipe data from S3
function getRecipe(key) {
    var params = {
        Bucket: bucket,
        Key: recipeFolderPath + key + ".json"
    };
    return s3.getObject(params).promise();
}
//Helper function to get recipe state data from S3
function getNewRecipeStateVariables() {
    var params = {
        Bucket: bucket,
        Key: recipeFolderPath + newRecipeStateFileName + ".json"
    };
    return s3.getObject(params).promise();
}
//Helper function to update recipe state data from S3
function updateNewRecipeStateVariables() {
    console.log("updating state recipe variables in s3");
    var data = {
		"spiceTotal": spiceTotal,
		"spiceCounter": spiceCounter,
		"currentAmount": currentAmount,
		"currentSpice": currentSpice,
		"startRecordingRecipe": startRecordingRecipe,
		"newRecipeName": newRecipeName
     };
    var params = {
        Bucket: bucket,
        Key: recipeFolderPath + newRecipeStateFileName + ".json",
        Body: JSON.stringify(data),
        ContentType: "application/json"
    };
    return s3.putObject(params).promise();
}
//Helper function that converts units to teaspoons
function convertToTeaspoon(units, value) {
    if(units.toLowerCase().includes("teaspoon")) {
        return value;
    }
    else if(units.toLowerCase().includes("tablespoon")) {
        return value*3;
    }
    else if(units.toLowerCase().includes("cup")) {
        return value*48;
    }
}
// --------------- Functions that control the skill's behavior -----------------------
function getWelcomeResponse(callback) {
    // If we wanted to initialize the session to have some attributes we could add those here.
    const sessionAttributes = {};
    const cardTitle = 'Welcome';
    const speechOutput = "Welcome to Spice Bot. How can I help you?";
    // If the user either does not reply to the welcome message or says something that is not
    // understood, they will be prompted again with this text.
    const repromptText = "How can I help you?";
    const shouldEndSession = false;
    callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
}
function handleTestRequest(intent, session, callback) {
    callback(session.attributes, buildSpeechletResponseWithoutCard("Hello, World!", "", "true"));
}
function handleSessionEndRequest(callback) {
    const cardTitle = 'Session Ended';
    const speechOutput = 'Thank you for trying the Alexa Skills Kit sample. Have a nice day!';
    // Setting this to true ends the session and exits the skill.
    const shouldEndSession = true;
    callback({}, buildSpeechletResponse(cardTitle, speechOutput, null, shouldEndSession));
}
function dispenseSpice(intent, session, callback) {
    const cardTitle = intent.name;
    const spice = intent.slots.Spice.value;
    const amount = intent.slots.Amount.value;
    const units = intent.slots.Units.value;
    let repromptText = '';
    let sessionAttributes = {};
    var amountConverted = convertToTeaspoon(units, amount);
    const shouldEndSession = false;
    let speechOutput = '';
    if (spice && amount) {
        var iotData = new awsIot.IotData({ endpoint: endpoint });
        var params = {
            topic: dispenseTopic,
            payload: '{ "spice": "' + spice + '", "tsp": "' + amountConverted + '"}',
            qos: 0
        };
        iotData.publish(params, function (err, data) {
            if (err) {
                console.log(err);
            }
            else {
                console.log("success");
                speechOutput = "Ok, dispensing " + amount + " " + units + " of " + spice;
                repromptText = "Please try again";
                callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
            }
        }); 
    } 
	else {
        speechOutput = "Please try again";
        repromptText = "Please try again";
        callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
    }
}
//dispense a whole recipe instead of just one spice
function dispenseRecipe(intent, session, callback) {
    const cardTitle = intent.name;
    let repromptText = '';
    let sessionAttributes = {};
    const shouldEndSession = false;
    let speechOutput = '';
	var key = intent.slots.Recipe.value;
	var params = {
	    Bucket: bucket,
	    Key: recipeFolderPath + key + ".json"
	};
    //check if recipe exists
    s3.headObject(params).on("success", function(response) {
        var getObjectPromise = getRecipe(key);
        if (key) {
    		getObjectPromise.then(function(data) {
    			//specify the rest end-point with which to publish to
    			var iotData = new awsIot.IotData({ endpoint: endpoint });
    			var params = {
    				topic: dispenseTopic,
    				payload: data,
    				qos: 0
    			};
    			iotData.publish(params, function (err, data) {
    				if (err) {
    					console.log(err);
    				}
    				else {
    					console.log("success");
    					speechOutput = "Ok, dispensing spices from recipe called " + key;
    					repromptText = "Please try again";
    					callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
    				}
    			});
    		});
        } 
    	else {
            speechOutput = "Please try again";
            repromptText = "Please try again";
            callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
        }
    }).on("error", function(error) {
        console.log(error.stack);
        speechOutput = "The recipe called " + key + " does not exist";
        repromptText = "Please try again";
        callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
    }).send();
}
//add recipe
function addRecipe(intent, session, callback) {
    const cardTitle = intent.name;
    const create = intent.slots.Create;
    const done = intent.slots.Done;
    const name = intent.slots.Name;
    const spiceRequest = intent.slots.Spice;
    const tspRequest = intent.slots.Teaspoon;
    let repromptText = '';
    let sessionAttributes = {};
    let shouldEndSession = false;
    let speechOutput = '';
    if (done.value =="done"){
      const cardTitle = intent.name;
      let repromptText = '';
      let speechOutput = '';
      var key = session.attributes.name;
      var data = {};
      var rec = [];
      var ingredients = session.attributes.recipes;
      for (var z in ingredients){
        var ing = {};
        var strz = ingredients[z];
        var spice_name = strz.split("|")[0];
        var num_tsp = strz.split("|")[1];
        console.log(strz);
        console.log(strz.split("|"));
        ing.spice = spice_name;
        ing.tsp = num_tsp;

        rec.push(ing);
      }
      data.recipe = rec;
      var params = {
        Bucket: bucket,
        Key: recipeFolderPath + key + ".json",
        Body: JSON.stringify(data),
        ContentType: "application/json"
      };
      s3.putObject(params, function (err, data) {
        if (err) { 
            console.log(err, err.stack); 
            
        } // an error occurred
        else { 
            speechOutput = "OK. Saving the recipe called "+ session.attributes.name;
            shouldEndSession = true;
            console.log(JSON.stringify(data)); 
            repromptText = "Please try again";
            callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
        }
      });
    }
    if (create.value == "create"){
      speechOutput = "OK. Specify the first ingredient and amount.";
      sessionAttributes.name = name.value;
      sessionAttributes.recipes = [];
      callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, null, shouldEndSession));
    }
    if (spiceRequest.value && tspRequest.value){
      speechOutput = "OK. Specify the next ingredient and amount.";
      var recipes = session.attributes.recipes;
      recipes.push(spiceRequest.value+ "|"+tspRequest.value);
      sessionAttributes.recipes = recipes;
      sessionAttributes.name = session.attributes.name;
      callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, null, shouldEndSession));
    }
}
    console.log("adding recipe");
    //initialize parameters
    const cardTitle = intent.name;
    let repromptText = '';
    let sessionAttributes = {};
    const shouldEndSession = false;
    let speechOutput = '';
    var key = intent.slots.Recipe.value;
    var params = {
	    Bucket: bucket,
	    Key: recipeFolderPath + key + ".json"
	};
	//check if recipe already exists in s3
    s3.headObject(params).on("success", function(response) {
        //get state data from s3
        var recipeStateVariablesPromise = getNewRecipeStateVariables();
        recipeStateVariablesPromise.then(function(stateData) {
            stateData = JSON.parse(stateData.Body.toString('utf-8'));
            spiceTotal = stateData.spiceTotal == undefined?0:stateData.spiceTotal;
            spiceCounter = stateData.spiceCounter== undefined?0:stateData.spiceCounter;
            currentSpice = stateData.currentSpice;
            currentAmount = stateData.currentAmount == undefined? 0:stateData.currentAmount;
            startRecordingRecipe = stateData.startRecordingRecipe;
            newRecipeName = stateData.newRecipeName;
            if(!startRecordingRecipe) {
                speechOutput = "The recipe called " + key + " already exists";
                repromptText = "Please try again";
                callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
            }
            else {
                console.log("attempting to get spice data for recipe");
                if (intent.slots.Spice.value && currentSpice != intent.slots.Spice.value && spiceCounter < spiceTotal) {
                    currentSpice = intent.slots.Spice.value;
                    console.log("current spice", currentSpice);
                    let updatedPromise = updateNewRecipeStateVariables();
                    updatedPromise.then(function(data) {
                        //request the first spice in the new recipe
                        let directive = {
                            "type": "Dialog.ElicitSlot",
                            "slotToElicit": "Amount"
                        };
                        speechOutput = "How many teaspoons of " + currentSpice + " would you like to add?";
                        let type = "PlainText";
                        let outputSpeech = {"type": type, "text": speechOutput};
            	        let response = {
            	            "outputSpeech": outputSpeech,
            	            "directives": [directive],
                            "shouldEndSession": false
            	        };
                        callback(sessionAttributes, response);
                    }).catch(function(err) {
                        console.log(err);
                    });
                }
                else if (intent.slots.Amount.value && spiceCounter < spiceTotal){
                    currentAmount = intent.slots.Amount.value;
                    updateNewRecipeStateVariables().then(function(data) {
                        if (currentSpice != undefined && currentAmount != 0 && spiceCounter < spiceTotal){
                            //add new spice to recipe
                            console.log("adding new spice to the recipe");
                            var getObjectPromise = getRecipe(key);
                            let data = {};
                            getObjectPromise.then(function (recipeData) { 
                                data = JSON.parse(recipeData.Body.toString('utf-8'));
                                data.recipe.push({"spice": currentSpice, "tsp": currentAmount});
                                console.log(JSON.stringify(data));
                                var params = {
                                    Bucket: bucket,
                                    Key: recipeFolderPath + newRecipeName + ".json",
                                    Body: JSON.stringify(data),
                                    ContentType: "application/json"
                                };
                                s3.putObject(params, function (err, data) {
                                    if (err) { 
                                        console.log(err, err.stack); // an error occurred   
                                    }
                                    else { 
                                        console.log("successfully. added spice to recipe");
                                        spiceCounter = parseInt(spiceCounter) + 1;
                                        if (spiceCounter == spiceTotal) {
                                            let tempRecipeName = newRecipeName;
                                            spiceTotal = 0;
                                            spiceCounter = 0;
                                            currentSpice = undefined;
                                            currentAmount = 0;
                                            startRecordingRecipe = false;
                                            newRecipeName = undefined;
                                            let updatedPromise = updateNewRecipeStateVariables();
                                            updatedPromise.then(function(data) {
                                                const cardTitle = intent.name;
                                                let speechOutput = 'Finished creating recipe called ' + tempRecipeName;
                                                let sessionAttributes = {};
                                                const shouldEndSession = false;
                                                let repromptText = 'Please try again';
                                                callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
                                            }).catch(function(err) {
                                              console.log(err);
                                            });
                                        }
                                        else {
                                            let updatedPromise = updateNewRecipeStateVariables();
                                            updatedPromise.then(function(data) {
                                                console.log("adding another spice");
                                                let directive = {
                                                    "type": "Dialog.ElicitSlot",
                                                    "slotToElicit": "Spice"
                                                };
                                                var count = parseInt(spiceCounter) + 1;
                                                speechOutput = "Added " + currentSpice + ". What is the <say-as interpret-as='ordinal'> " + count + "</say-as> spice you would like to add?";
                                                let type = "SSML";
                                                let outputSpeech = {"type": type, "ssml": "<speak>" + speechOutput + "</speak>"};
                                    	        let response  = {
                                    	            "outputSpeech": outputSpeech,
                                    	            "directives": [directive],
                                                    "shouldEndSession": false
                                    	        };
                                    	        callback(sessionAttributes, response);
                                            });
                                        }
                                    }
                                });
                            }).catch(function(err) {
                                    console.log(err, err.stack); //failed to get data from recipe
                            });
                        }
                    });
                }
            }
        });
    }).on("error", function(error) {
        var recipeStateVariablesPromise = getNewRecipeStateVariables();
        recipeStateVariablesPromise.then(function(stateData){
            //get state data from s3
            console.log("parsing data recipe state data");
            stateData = JSON.parse(stateData.Body.toString('utf-8'));
            spiceTotal = stateData.spiceTotal;
            spiceCounter = stateData.spiceCounter;
            currentSpice = stateData.currentSpice;
            currentAmount = stateData.currentAmount;
            startRecordingRecipe = stateData.startRecordingRecipe;
            newRecipeName = stateData.newRecipeName;
            console.log("completed paresing recipe state data for first recipe");
            startRecordingRecipe = true;
            newRecipeName = key;
            spiceTotal = intent.slots.SpiceCount.value;
            //create an empty recipe
            let data = {"recipe":[]};
            var params = {
                Bucket: bucket,
                Key: recipeFolderPath + newRecipeName + ".json",
                Body: JSON.stringify(data),
                ContentType: "application/json"
            };
            s3.putObject(params, function (err, data) {
                if (err) { 
                    console.log(err, err.stack);// an error occurred
                } 
                else { 
                    console.log("Created new empty recipe");
                    //update recipe state data
                    let updatedPromise = updateNewRecipeStateVariables();
                    updatedPromise.then(function(data) {
                        console.log("sending request for first spice in recipe");
                         //request the first spice in the new recipe
                        let directive = {
                            "type": "Dialog.ElicitSlot",
                            "slotToElicit": "Spice"
                        };
                        speechOutput = "What is the first spice you would like to add to " + newRecipeName + "?";
                        let type = "PlainText";
                        let outputSpeech = {"type": type, "text": speechOutput};
            	        let response  = {
            	            "outputSpeech": outputSpeech,
            	            "directives": [directive],
                            "shouldEndSession": false
            	        };
                        callback(sessionAttributes, response);
                    }).catch(function(err) {
                      console.log(err);
                    });
                }
            });
        });
    }).send();
}
//delete recipe
function deleteRecipe(intent, session, callback) {
    const cardTitle = intent.name;
    let repromptText = '';
    let sessionAttributes = {};
    const shouldEndSession = false;
    let speechOutput = '';
    var key = intent.slots.Recipe.value;
    var params = {
	    Bucket: bucket,
	    Key: recipeFolderPath + key + ".json"
	};
    s3.headObject(params).on("success", function(response) {
        if(key) {
            var params = {
                Bucket: bucket,
                Key: recipeFolderPath + key + ".json"
            };
            s3.deleteObject(params, function (err, data) {
                if (err) {
                    console.log(err, err.stack); // an error occurred
                }
                else 
                {
                    speechOutput = "Ok, deleted recipe called " + key;
                    repromptText = "Please try again";
                    callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
                }
            });
        }
        else {
                speechOutput = "The recipe called " + key + " does not exist.";
                repromptText = "Please try again";
                callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
        }
    }).on("error", function(error) {
         console.log(error.stack);
        speechOutput = "The recipe called " + key + " does not exist";
        repromptText = "Please try again";
        callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
    }).send();
}
//read recipe
function readRecipe(intent, session, callback) {
    const cardTitle = intent.name;
    let repromptText = '';
    let sessionAttributes = {};
    const shouldEndSession = false;
    let speechOutput = '';
    var key = intent.slots.Recipe.value;
    var params = {
	    Bucket: bucket,
	    Key: recipeFolderPath + key + ".json"
	};
    s3.headObject(params).on("success", function(response) {
        var getObjectPromise = getRecipe(key);
        var data = {};
        getObjectPromise.then(function (recipeData) {
            data = JSON.parse(recipeData.Body.toString('utf-8'));
            var array = data.recipe;
            var string = "";
            for (var i = 0; i < array.length;i++) {
                if(i != array.length-1)
                {
                    string += array[i].tsp + " teaspoons of " + array[i].spice + ", ";
                }
                else
                {
                    string += "and " + array[i].tsp + " teaspoons of " + array[i].spice + ".";
                }
            }
            if(string != "") {
                string = "The recipe called " + intent.slots.Recipe.value + " contains " + string;
                speechOutput = string;
                repromptText = "Please try again";
                callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
            }
            else {
                speechOutput = "The recipe is empty";
                repromptText = "Please try again";
                callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
            }
        }).catch(function(err) {
    		console.log(err, err.stack); 
        });
    }).on("error", function(error) {
         console.log(error.stack);
        speechOutput = "The recipe called " + key + " does not exist";
        repromptText = "Please try again";
        callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
    }).send();
}
//add spice
function addSpiceToRecipe(intent, session, callback) {
    const cardTitle = intent.name;
    let repromptText = '';
    let sessionAttributes = {};
    const shouldEndSession = false;
    let speechOutput = '';
    var spice = intent.slots.Spice.value;
    var amount = intent.slots.Amount.value;
    var units = intent.slots.Units.value;
    var amountConverted = convertToTeaspoon(units, amount);
    var key = intent.slots.Recipe.value;
    var params = {
	    Bucket: bucket,
	    Key: recipeFolderPath + key + ".json"
	};
    s3.headObject(params).on("success", function(response) {
        var getObjectPromise = getRecipe(key);
        var data = {};
        getObjectPromise.then(function (recipeData) { 
            data = JSON.parse(recipeData.Body.toString('utf-8'));
            data.recipe.push({spice: spice, tsp: amountConverted});
            var params = {
                Bucket: bucket,
                Key: recipeFolderPath + key + ".json",
                Body: JSON.stringify(data),
                ContentType: "application/json"
            };
            s3.putObject(params, function (err, data) {
                if (err) { 
                    console.log(err, err.stack); // an error occurred   
                }
                else { 
                    speechOutput = "Ok, added " + amount + " " + units + " of " + spice + " to " + key;
                    repromptText = "Please try again";
                    callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
                }
            });
        }).catch(function(err) {
                console.log(err, err.stack); 
        });
    }).on("error", function(error) {
         console.log(error.stack);
        speechOutput = "The recipe called " + key + " does not exist";
        repromptText = "Please try again";
        callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
    }).send();
}
//remove spice
function deleteSpiceFromRecipe(intent, session, callback) {
    const cardTitle = intent.name;
    let repromptText = '';
    let sessionAttributes = {};
    const shouldEndSession = false;
    let speechOutput = '';
    var spice = intent.slots.Spice.value;
    var key = intent.slots.Recipe.value;
    var params = {
	    Bucket: bucket,
	    Key: recipeFolderPath + key + ".json"
	};
    s3.headObject(params).on("success", function(response) {
        var getObjectPromise = getRecipe(key);
        var data = {};
        getObjectPromise.then(function (recipeData) { 
            data = JSON.parse(recipeData.Body.toString('utf-8'));
            var newList = [];
            for (var i = 0;i < data.recipe.length; i++)
            {
                if(data.recipe[i].spice != spice)
                {
                    newList.push(data.recipe[i]);
                }
            }
            data.recipe = newList;
            var params = {
                Bucket: bucket,
                Key: recipeFolderPath + key + ".json",
                Body: JSON.stringify(data),
                ContentType: "application/json"
            };
            s3.putObject(params, function (err, data) {
                if (err) { 
                    console.log(err, err.stack);  // an error occurred
                }
                else {
                    speechOutput = "Ok, removed " + spice + " from " + key;
                    repromptText = "Please try again";
                    callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
                }
            });
        }).catch(function(err) {
                console.log(err, err.stack); 
        });
    }).on("error", function(error) {
         console.log(error.stack);
        speechOutput = "The recipe called " + key + " does not exist";
        repromptText = "Please try again";
        callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
    }).send();
}
//update a spice quantity
function updateSpiceAmountInRecipe(intent, session, callback) {
    const cardTitle = intent.name;
    let repromptText = '';
    let sessionAttributes = {};
    const shouldEndSession = false;
    let speechOutput = '';
    var key = intent.slots.Recipe.value;
    var params = {
	    Bucket: bucket,
	    Key: recipeFolderPath + key + ".json"
	};
    s3.headObject(params).on("success", function(response) {
        var spice = intent.slots.Spice.value;
        var amount = intent.slots.Amount.value;
        var units = intent.slots.Units.value;
        var amountConverted = convertToTeaspoon(units, amount);
    	var oldValue = "";
        var params = {
            Bucket: bucket,
            Key: recipeFolderPath + key + ".json"
        };
        var getObjectPromise = getRecipe(key);
    	var data = {};
    	getObjectPromise.then(function(recipeData){
    		data = JSON.parse(recipeData.Body.toString('utf-8'));
            for (var i = 0;i < data.recipe.length; i++)
            {
                if(data.recipe[i].spice == spice)
                {
    				oldValue = data.recipe[i].tsp;
                    data.recipe[i].tsp = amountConverted;
                }
            }
    		//overwrite recipe with new data
    		s3.putObject(params, function (err, data) {
                if (err) { 
                    console.log(err, err.stack);  // an error occurred
                }
                else {
                    speechOutput = "Ok, updated amount of " + spice + " from " + oldValue + " teaspoons to " + amount + " " + units + " in recipe called " + key;
                    repromptText = "Please try again";
                    callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
                }
            });
    	});
    }).on("error", function(error) {
        console.log(error.stack);
        speechOutput = "The recipe called " + key + " does not exist";
        repromptText = "Please try again";
        callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
    }).send();
}
//send email or text message with Amazon SNS
function sendSMS(context, message) {
    var sns = new awsIot.SNS();
    var params = {
        Message: message,
        TopicArn: topicARN
    };
    sns.publish(params, context.done);
}