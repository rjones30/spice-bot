#Implementation Instructions
In order to implement the Node.js code, a user must setup

1. An S3 bucket in Amazon S3
2. The folder path in the S3 bucket to contain the JSON recipe files
3. An Amanzon SNS topic that sends email/text to a users phone
4. An Alexa Skill that passes intent requests to the lambda function
5. Create a "Thing" in AWS IoT Core that represents the raspberry pi device

There are five variables defined in the first few lines of the lambda function that must be set in order for the lambda function to work. These values are:

```
#!js
var bucket = "unique-bucket-name";
var recipeFolderPath = "recipes/";
var endpoint = "amazon-endpint";
var dispenseTopic = "test";
var topicARN = "amazon-topic-arn"
```

**bucket** is the name of the S3 bucket created
**recipeFolderPath** is the folder path within the S3 bucket in which to store the recipe JSON files
**endpoint** is the name of the AWS IoT REST endpoint used to publish topic messages to the raspberry pi
**dispenseTopic** is the name of the topic the dispense commands and published on
**topicARN** is the name of the Amazon SNS topic to publish to in order to send email and text message alerts to users
